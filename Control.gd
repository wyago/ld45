extends Node

var controlled: RigidBody2D
var thrust: Node2D

var amount = 0
var last_visible = false
onready var puff = preload("res://Puff.tscn")

func _ready():
	controlled = get_parent()
	thrust = $"../Thrust/Line"
	
func make_puff():
	var instance = puff.instance()
	var vector = Vector2(cos(controlled.rotation), sin(controlled.rotation))
	instance.position = $"../Smokepoint".global_position + Vector2(rand_range(-5, 5), rand_range(-5,5))
	instance.linear_velocity = controlled.linear_velocity - vector*amount*2 + Vector2(rand_range(-15, 15), rand_range(-15,15))
	instance.scaling = amount * 0.02 + 0.1
	$"../..".add_child(instance)

func _physics_process(delta):
	var vector = Vector2(cos(controlled.rotation), sin(controlled.rotation))
	var side_vector = Vector2(cos(controlled.rotation + PI / 2), sin(controlled.rotation + PI / 2))
	
	if Input.is_action_pressed("accellerate_forward"):
		amount = lerp(amount, 100, delta)
		controlled.apply_central_impulse(vector * amount * delta * 0.5)
		thrust.visible = true
		last_visible = true
	else:
		if last_visible:
			make_puff()
			make_puff()
			make_puff()
			make_puff()
			make_puff()
		last_visible = false
		amount = 0
		thrust.visible = false
		
	if Input.is_action_pressed("rotate_left"):
		controlled.apply_torque_impulse(-delta * 10000)
	if Input.is_action_pressed("rotate_right"):
		controlled.apply_torque_impulse(delta * 10000)