extends RigidBody2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

var scaling = 1

# Called when the node enters the scene tree for the first time.
func _ready():
	angular_velocity = -10 if randf() > 0.5 else 10

var time = 0
func _process(delta):
	time += delta
	var factor = time * 0.8
	$Line2D.default_color = Color(0.2 + (1 - factor) * 0.2, 0.2, 0.2, 1 - factor)
	var scale = (sin(factor * 2) + 0.2) * scaling
	$Line2D.scale = Vector2(scale, scale)
	if factor > 1:
		queue_free()
