extends Node2D

var points = []
var lasts = []
var distances = []

export(float) var minimum_distance = 0.5
export(float) var maximum_distance = 1.5
export(int) var point_count = 100
export(Color) var albedo = Color(0.8, 0.7, 0.75)

# Called when the node enters the scene tree for the first time.
func _ready():
	points.resize(point_count)
	lasts.resize(point_count)
	distances.resize(point_count)
	var size = get_viewport().size + Vector2(0, 100)
	for i in range(0,point_count):
		points[i] = Vector2(rand_range(-size.x, size.x), rand_range(-size.y, size.y))
		lasts[i] = points[i]
		distances[i] = rand_range(minimum_distance, maximum_distance)

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func _draw():
	var size = get_viewport().size + Vector2(0, 100)
	for i in range(0, point_count):
		var dist_factor = distances[i] - 0.5
		if lasts[i].distance_to(points[i]) < 1:
			lasts[i] -= Vector2(1,1)
		if (lasts[i] - points[i]).length_squared() < 100*100:
			draw_line(lasts[i], points[i], albedo * dist_factor, 1, false)
		lasts[i] = points[i]
		
		if points[i].x < -size.x:
			points[i].x = size.x
		if points[i].x > size.x:
			points[i].x = -size.x
		if points[i].y > size.y:
			points[i].y = -size.y
		if points[i].y < -size.y:
			points[i].y = size.y

func move(vector):
	update()
	for i in range(0, point_count):
		points[i] += vector * distances[i]