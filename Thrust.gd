extends Node2D
var line: Line2D

var acc = 0
var amount = 0

func _ready():
	line = $Line

func _process(delta):
	acc -= delta

	if acc < 0:
		acc = 0.02
		line.default_color = Color(rand_range(0.5, 0.8), rand_range(0.3, 0.5), rand_range(0.2, 0.3))
		line.set_point_position(1, Vector2(rand_range(-10 * amount, -50 * amount), rand_range(-3, 3)))

func _physics_process(delta):
	if line.visible:
		amount = lerp(amount, 1, 0.008)
	else:
		amount = 0